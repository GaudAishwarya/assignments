var Vehicle = /** @class */ (function () {
    function Vehicle() {
    }
    return Vehicle;
}());
var DisplayProperties = /** @class */ (function () {
    function DisplayProperties(reg_num, model, owner, num_of_wheel) {
        console.log("Entered in DisplayProperties class:");
        this.reg_num = reg_num;
        this.model = model;
        this.owner = owner;
        this.num_of_wheel = num_of_wheel;
        console.log("Vehicle properties are: %s,%s,%s,%d", this.reg_num, this.model, this.owner, this.num_of_wheel);
    }
    return DisplayProperties;
}());
var Two_obj = { reg_num: "MH17-7766", model: "Activa-5G", owner: "Miss Gaud", num_of_wheel: 2 };
//console.log("Twowheeler properties are="+Two_obj.reg_num,Two_obj.model,Two_obj.owner,Two_obj.num_of_wheel);
var Three_obj = { reg_num: "MH15-3456", model: "Autorickshaw", owner: "Miss minion", num_of_wheel: 3 };
//console.log("Threewheeler properties are="+Three_obj.reg_num,Three_obj.model,Three_obj.owner,Three_obj.num_of_wheel);
var Four_obj = { reg_num: "MH21-7890", model: "Audi", owner: "Mr abc", num_of_wheel: 4 };
//console.log("Fourwheeler properties are="+Four_obj.reg_num,Four_obj.model,Four_obj.owner,Four_obj.num_of_wheel);
var Two = new DisplayProperties(Two_obj.reg_num, Two_obj.model, Two_obj.owner, Two_obj.num_of_wheel);
var Three = new DisplayProperties(Three_obj.reg_num, Three_obj.model, Three_obj.owner, Three_obj.num_of_wheel);
var Four = new DisplayProperties(Four_obj.reg_num, Four_obj.model, Four_obj.owner, Four_obj.num_of_wheel);
