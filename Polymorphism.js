var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Vehicle = /** @class */ (function () {
    function Vehicle(reg_num, model, owner) {
        this.reg_num = reg_num;
        this.model = model;
        this.owner = owner;
    }
    Vehicle.prototype.disp = function () {
        console.log("Entered in Vehicle class");
    };
    return Vehicle;
}());
var Twowheeler = /** @class */ (function (_super) {
    __extends(Twowheeler, _super);
    function Twowheeler(reg_num, model, owner, num_of_wheel) {
        var _this = _super.call(this, reg_num, model, owner) || this;
        _this.num_of_wheel = num_of_wheel;
        return _this;
    }
    Twowheeler.prototype.disp = function () {
        console.log("Entered in TwoWheeler class");
        console.log("Twowheeler properties are %s,%s,%d", this.reg_num, this.model, this.num_of_wheel);
    };
    return Twowheeler;
}(Vehicle));
var Threewheeler = /** @class */ (function (_super) {
    __extends(Threewheeler, _super);
    function Threewheeler(reg_num, model, owner, num_of_wheel) {
        var _this = _super.call(this, reg_num, model, owner) || this;
        _this.num_of_wheel = num_of_wheel;
        return _this;
    }
    Threewheeler.prototype.disp = function () {
        console.log("Entered in ThreeWheeler class");
        console.log("Threewheeler properties are %s,%s,%d", this.reg_num, this.model, this.num_of_wheel);
    };
    return Threewheeler;
}(Vehicle));
var Fourwheeler = /** @class */ (function (_super) {
    __extends(Fourwheeler, _super);
    function Fourwheeler(reg_num, model, owner, num_of_wheel) {
        var _this = _super.call(this, reg_num, model, owner) || this;
        _this.num_of_wheel = num_of_wheel;
        return _this;
    }
    Fourwheeler.prototype.disp = function () {
        console.log("Entered in FourWheeler class");
        console.log("Fourwheeler properties are %s,%s,%d", this.reg_num, this.model, this.num_of_wheel);
    };
    return Fourwheeler;
}(Vehicle));
var Two = new Twowheeler("MH17-1234", "Activa-5g", "Miss Gaud", 2);
var Three = new Threewheeler("MH15-3456", "Autorickshaw", "Miss minion", 3);
var Four = new Fourwheeler("MH21-7890", "Audi", "Mr abc", 4);
Two.disp();
Three.disp();
Four.disp();
