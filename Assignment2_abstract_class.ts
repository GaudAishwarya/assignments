abstract class Vehicle
{
	 reg_num:string
	 model:string
     owner:string
	constructor(public reg_num1:string,public model1:string,public owner1:string)
	{
	 this.reg_num=reg_num1
	 this.model=model1
	 this.owner=owner1
	}
	
	abstract disp():void;
}


class Twowheeler extends Vehicle
{
	
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 console.log("Entered in TwoWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	}
	disp():void
	{
		console.log("Vehicle properties of TwoWheeler are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)
	}

}

class Threewheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 console.log("Entered in ThreeWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	}
		disp():void
	{
		  console.log("Vehicle properties of ThreeWheeler are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)
	}

	
}

class Fourwheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 console.log("Entered in FourWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	 
	}
		disp():void
	{
						
		console.log("Vehicle properties of FourWheeler are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)


	}

	
}


var Two=new Twowheeler("MH17-1234","Activa-5g","Miss Gaud",2)
Two.disp()
var Three=new Threewheeler("MH15-3456","Autorickshaw","Miss minion",3)
Three.disp()
var Four=new Fourwheeler("MH21-7890","Audi","Mr abc",4)
//Two.disp()
Four.disp()



