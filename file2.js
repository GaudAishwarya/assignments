// <reference path="typings/node/node.d.ts" />
// <reference path="typings/typescript/typescript.d.ts" />
// <reference path="home/sumit/.cache/yarn/v1/npm-tslint-3.15.1-da165ca93d8fdc2c086b51165ee1bacb48c98ea5/typings/node/node.d.ts" />
var MyClass = /** @class */ (function () {
    function MyClass() {
        // Here we import the File System module of node
        this.fs = require('fs');
    }
    MyClass.prototype.createFile = function () {
        this.fs.writeFile('foo.txt', 'I am cool!', function (err) {
            if (err) {
                return console.error(err);
            }
            console.log("File created and the content has been written!");
        });
    };
    MyClass.prototype.showFile = function () {
        this.fs.readFile('foo1.txt', function (err, data) {
            if (err) {
                return console.error(err);
            }
            console.log("read content: " + data.toString());
        });
    };
    return MyClass;
}());
var obj = new MyClass();
obj.createFile();
obj.showFile();
