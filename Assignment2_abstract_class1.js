var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Vehicle = /** @class */ (function () {
    //reg_num:string
    //model:string
    //owner:string
    function Vehicle(reg_num, model, owner) {
        this.reg_num = reg_num;
        this.model = model;
        this.owner = owner;
        //this.reg_num=reg_num1
        //this.model=model1
        //this.owner=owner1
    }
    return Vehicle;
}());
var Twowheeler = /** @class */ (function (_super) {
    __extends(Twowheeler, _super);
    function Twowheeler(reg_num, model, owner, num_of_wheel) {
        var _this = this;
        console.log("Entered in TwoWheeler class:");
        _this = _super.call(this, reg_num, model, owner) || this;
        _this.reg_num = reg_num;
        _this.model = model;
        _this.owner = owner;
        _this.num_of_wheel = num_of_wheel;
        return _this;
        //super.disp(reg_num,model,owner,num_of_wheel)
    }
    Twowheeler.prototype.disp = function () {
        //console.log("Vehicle properties of TwoWheeler are: %s,%s,%s,%d",reg_num,model,owner,num_of_wheel)
        console.log("Vehicle properties of TwoWheeler are: %s,%s,%s,%d", this.reg_num, this.model, this.owner, this.num_of_wheel);
    };
    return Twowheeler;
}(Vehicle));
/*
class Threewheeler extends Vehicle
{
    num_of_wheel:number
    constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
    {
    console.log("Entered in ThreeWheeler class:")
     super(reg_num,model,owner)
     this.reg_num=reg_num
     this.model=model
     this.owner=owner

     this.num_of_wheel=num_of_wheel
     //super.disp(reg_num,model,owner,num_of_wheel)
    }
        disp():void
    {
                console.log("Vehicle properties of TwoWheeler are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)
    }

    
}

class Fourwheeler extends Vehicle
{
    num_of_wheel:number
    constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
    {
     console.log("Entered in FourWheeler class:")
     super(reg_num,model,owner)
     this.reg_num=reg_num
     this.model=model
     this.owner=owner

     this.num_of_wheel=num_of_wheel
     //super.disp(reg_num,model,owner,num_of_wheel)
    }
        disp():void
    {
                        
        console.log("Vehicle properties of TwoWheeler are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)


    }

    
}
*/
var Two = new Twowheeler("MH17-1234", "Activa-5g", "Miss Gaud", 2);
Two.disp();
//var Three=new Threewheeler("MH15-3456","Autorickshaw","Miss minion",3)
//var Four=new Fourwheeler("MH21-7890","Audi","Mr abc",4)
