
class Explore_overload_fun
{
   disp(name:string|string[]|number|boolean) { 
   if(typeof name == "string") { 
      console.log("single string="+name) 
   }
   else if (typeof name == "number") {
     console.log("number val="+name); 
   
   } 
   else if (typeof name == "boolean") {
      if(name==true)
      {
     console.log("boolean val="+name); 
       }
   } 

   else if (name instanceof Array)
   {
     if(name.every(function(i){ return typeof i === "string" }))
     {
        console.log("string array");
        var s1:string=""
        for (var i = 0; i<name.length;i++) {
              s1=s1+" "+name[i];
        }
       
        console.log("concatenation of string array="+s1);
     } 
}
}
}

var obj_exp=new Explore_overload_fun()
obj_exp.disp("Animals")
obj_exp.disp(["Tiger","Tommy","Doggy","Cat"])
obj_exp.disp(17)
obj_exp.disp(true)
