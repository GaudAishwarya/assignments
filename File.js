"use strict";
exports.__esModule = true;
// <reference path="typings/node/node.d.ts" />
// <reference path="typings/typescript/typescript.d.ts" />
// <reference path="home/sumit/.cache/yarn/v1/npm-tslint-3.15.1-da165ca93d8fdc2c086b51165ee1bacb48c98ea5/typings/node/node.d.ts" />
var fs1 = require("fs");
var MyClass = /** @class */ (function () {
    //import  fs1 = require('fs');
    function MyClass(file1, write_file) {
        // Here we import the File System module of node
        this.fs = require('fs');
        this.file1 = file1;
        this.write_file = write_file;
    }
    MyClass.prototype.showFile = function () {
        this.fs.readFile(this.file1, function (err, data) {
            if (err) {
                return console.error(err);
            }
            console.log("read content: " + data.toString());
            var s1 = data.toString();
            console.log("s1=" + s1);
            var splitted = s1.split("\n");
            console.log("splitted string and len=" + splitted, splitted.length);
            for (var i = 0; i < splitted.length; i++) {
                // code...
                splitted[i] = splitted[i].toUpperCase();
            }
            console.log("Converted string=" + splitted);
            //var s2:string=this.file2
            //const file=fs1.createWriteStream(this.write_file)
            var file = fs1.createWriteStream('File_new.txt');
            for (var i = 0; i < splitted.length; i++) {
                file.write(splitted[i]);
                file.write("\n");
            }
        });
    };
    return MyClass;
}());
var obj = new MyClass('foo1.txt', 'File_new.txt');
obj.showFile();
