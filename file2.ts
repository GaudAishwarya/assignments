// <reference path="typings/node/node.d.ts" />
// <reference path="typings/typescript/typescript.d.ts" />
// <reference path="home/sumit/.cache/yarn/v1/npm-tslint-3.15.1-da165ca93d8fdc2c086b51165ee1bacb48c98ea5/typings/node/node.d.ts" />

class MyClass {

    // Here we import the File System module of node
    private fs = require('fs');

    constructor() { }

    createFile() {

        this.fs.writeFile('foo.txt', 'Hello everyone welcome to iauro!',  function(err) {
            if (err) {
                return console.error(err);
            }
            console.log("File created and the content has been written!");
        });
    }

    showFile() {

        this.fs.readFile('foo1.txt', function (err, data) {
            if (err) {
                return console.error(err);
            }
            console.log("read content: " + data.toString());
        });
    }
}

var obj = new MyClass();
obj.createFile();
obj.showFile();