
//Note:We can't create an object of an interface as well as abstract class
class Vehicle {
	 reg_num:string;
	 model:string;
     owner:string;
	
}


interface vehicleDetails {
	disp_details();
}

interface Twowheeler extends Vehicle
{
	num_of_wheel:number;
	// disp_prop();
	
}

interface Threewheeler extends Vehicle
{
	num_of_wheel:number;
	// disp_prop();
	
}

interface Fourwheeler extends Vehicle
{
	num_of_wheel:number;
	// disp_prop();
	
}



class DisplayProperties implements Twowheeler,Threewheeler,Fourwheeler, vehicleDetails
{
	 reg_num:string
	 model:string
     owner:string
     num_of_wheel:number

	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 // console.log("Entered in DisplayProperties class:")
	 this.reg_num=reg_num
	 this.model=model
	 this.owner=owner
	 this.num_of_wheel=num_of_wheel
	 //console.log("Vehicle properties are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)
    }

	/*disp_prop()
	{
		console.log("Vehicle properties are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel)

	}*/

	disp_details(){
		console.info("Vehicle properties are: %s,%s,%s,%d",this.reg_num,this.model,this.owner,this.num_of_wheel);
	}


}



// let Two_obj: Twowheeler = {reg_num: "MH17-7766" , model: "Activa-5G", owner: "Miss Gaud",num_of_wheel: 2};
//console.log("Twowheeler properties are="+Two_obj.reg_num,Two_obj.model,Two_obj.owner,Two_obj.num_of_wheel);

//let Three_obj: Threewheeler = {reg_num: "MH15-3456" , model: "Autorickshaw", owner: "Miss minion",num_of_wheel: 3};
//console.log("Threewheeler properties are="+Three_obj.reg_num,Three_obj.model,Three_obj.owner,Three_obj.num_of_wheel);

//let Four_obj: Fourwheeler = {reg_num: "MH21-7890" , model: "Audi", owner: "Mr abc",num_of_wheel: 4};
//console.log("Fourwheeler properties are="+Four_obj.reg_num,Four_obj.model,Four_obj.owner,Four_obj.num_of_wheel);

// var Two=new DisplayProperties(Two_obj.reg_num,Two_obj.model,Two_obj.owner,Two_obj.num_of_wheel)
// Two.disp_prop();


/*var Three=new DisplayProperties(Three_obj.reg_num,Three_obj.model,Three_obj.owner,Three_obj.num_of_wheel)
Three.disp_prop();
var Four=new DisplayProperties(Four_obj.reg_num,Four_obj.model,Four_obj.owner,Four_obj.num_of_wheel)
Four.disp_prop();*/

let CustomerTwoWheeler = new DisplayProperties("MH15-3456"
, "Activa-5G"
, "Miss Gaud"
, 2
);

console.info(CustomerTwoWheeler);

CustomerTwoWheeler.disp_details();
 
