class Vehicle
{
	reg_num:string
	model:string
    owner:string
	constructor(reg_num:string,model:string,owner:string)
	{
	this.reg_num=reg_num
	this.model=model
	this.owner=owner
	}
	disp(reg_num:string,model:string,owner:string,num_of_wheel:number):void
	{
	    console.log("Vehicle properties are: %s,%s,%s,%d",reg_num,model,owner,num_of_wheel)
	}
}


class Twowheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 console.log("Entered in TwoWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	 super.disp(reg_num,model,owner,num_of_wheel)
	}

}

class Threewheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	console.log("Entered in ThreeWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	 super.disp(reg_num,model,owner,num_of_wheel)
	}
	
}

class Fourwheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 console.log("Entered in FourWheeler class:")
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	 super.disp(reg_num,model,owner,num_of_wheel)
	}
	
}


var Two=new Twowheeler("MH17-1234","Activa-5g","Miss Gaud",2)
var Three=new Threewheeler("MH15-3456","Autorickshaw","Miss minion",3)
var Four=new Fourwheeler("MH21-7890","Audi","Mr abc",4)


