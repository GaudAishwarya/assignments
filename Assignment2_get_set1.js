//Run using below commands::
//tsc --target es6 Assignment2_get_set.ts 
//node Assignment2_get_set.js 
class Vehicle {
    constructor(reg_num, model, owner) {
        this._fullName = '';
        this.reg_num = reg_num;
        this.model = model;
        this.owner = owner;
    }
    get fullName() {
        return this._fullName;
    }
    set fullName(name) {
        this._fullName = name;
    }
    disp(reg_num, model, owner, num_of_wheel) {
        console.log("Vehicle properties are: %s,%s,%s,%d", reg_num, model, owner, num_of_wheel);
    }
}
class Twowheeler extends Vehicle {
    constructor(reg_num, model, owner, num_of_wheel) {
        console.log("Entered in TwoWheeler class:");
        super(reg_num, model, owner);
        this.num_of_wheel = num_of_wheel;
        super.disp(reg_num, model, owner, num_of_wheel);
    }
}
class Threewheeler extends Vehicle {
    constructor(reg_num, model, owner, num_of_wheel) {
        console.log("Entered in ThreeWheeler class:");
        super(reg_num, model, owner);
        this.num_of_wheel = num_of_wheel;
        super.disp(reg_num, model, owner, num_of_wheel);
    }
}
class Fourwheeler extends Vehicle {
    constructor(reg_num, model, owner, num_of_wheel) {
        console.log("Entered in FourWheeler class:");
        super(reg_num, model, owner);
        this.num_of_wheel = num_of_wheel;
        super.disp(reg_num, model, owner, num_of_wheel);
    }
}
var Two = new Twowheeler("MH17-1234", "Activa-5g", "Miss Gaud", 2);
Two.fullName = "Miss Aishwarya";
console.log("Full name of owner=" + Two.fullName);
var Three = new Threewheeler("MH15-3456", "Autorickshaw", "Miss minion", 3);
var Four = new Fourwheeler("MH21-7890", "Audi", "Mr abc", 4);
