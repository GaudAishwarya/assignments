var Solution = /** @class */ (function () {
    function Solution() {
    }
    Solution.prototype.find_max = function (store_arr) {
        var maxi = store_arr[0];
        for (var i = 1; i < store_arr.length; i++) {
            if (maxi < store_arr[i]) {
                maxi = store_arr[i];
            }
        }
        return maxi;
    };
    Solution.prototype.increase = function (x, store_arr) {
        store_arr[x - 1] = store_arr[x - 1] + 1;
    };
    Solution.prototype.maxcounter = function (x, store_arr) {
        store_arr[x] = this.find_max(store_arr);
    };
    Solution.prototype.solution = function (arr_name, n) {
        var store_arr = new Array(n);
        for (var i = 0; i < store_arr.length; i++) {
            store_arr[i] = 0;
        }
        console.log("Given array=" + arr_name);
        console.log("Initialized counter array=" + store_arr);
        for (var k = 0; k < arr_name.length; k++) {
            for (var x = 0; x < n; x++) {
                if (arr_name[k] == x) {
                    this.increase(x, store_arr);
                }
                if (arr_name[k] == (store_arr.length + 1)) {
                    this.maxcounter(x, store_arr);
                }
            }
            console.log(store_arr);
        }
        console.log("Final result obtained=" + store_arr);
    };
    return Solution;
}());
var arr_name;
arr_name = [3, 4, 4, 6, 1, 4, 4];
var n = 5;
var result = new Solution();
result.solution(arr_name, n);
