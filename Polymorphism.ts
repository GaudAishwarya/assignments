class Vehicle
{
	reg_num:string
	model:string
    owner:string
	constructor(reg_num:string,model:string,owner:string)
	{
	this.reg_num=reg_num
	this.model=model
	this.owner=owner
	}
	disp():void
	{
	console.log("Entered in Vehicle class")
	}
}


class Twowheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	}
	disp():void
	{
	console.log("Entered in TwoWheeler class")
	console.log("Twowheeler properties are %s,%s,%d",this.reg_num,this.model,this.num_of_wheel)
	
	}
}

class Threewheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	}
	disp():void
	{
	console.log("Entered in ThreeWheeler class")
	console.log("Threewheeler properties are %s,%s,%d",this.reg_num,this.model,this.num_of_wheel)
	
	}
}

class Fourwheeler extends Vehicle
{
	num_of_wheel:number
	constructor(reg_num:string,model:string,owner:string,num_of_wheel:number)
	{
	 super(reg_num,model,owner)
	 this.num_of_wheel=num_of_wheel
	}
	disp():void
	{
	console.log("Entered in FourWheeler class")
	console.log("Fourwheeler properties are %s,%s,%d",this.reg_num,this.model,this.num_of_wheel)
	
	}
}


var Two=new Twowheeler("MH17-1234","Activa-5g","Miss Gaud",2)
var Three=new Threewheeler("MH15-3456","Autorickshaw","Miss minion",3)
var Four=new Fourwheeler("MH21-7890","Audi","Mr abc",4)
Two.disp()
Three.disp()
Four.disp()


